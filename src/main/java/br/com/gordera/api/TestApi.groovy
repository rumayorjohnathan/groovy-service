package br.com.gordera.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import br.com.gordera.service.ITestService

@RestController
@RequestMapping("/v1/sample")
class TestApi {

	@Autowired
	private ITestService testService;
	
}
